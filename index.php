<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>ITDB Services</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.ico" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: NewBiz
    Theme URL: https://bootstrapmade.com/newbiz-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">
        <div>
            <div class="float-left">
                <!-- Uncomment below if you prefer to use an image logo -->
                <img src="img/it_logo.png" width="82px" height="69px">
            </div>
            <div class="float-left" style="margin-left: 15px;margin-top: 10px;">
                <a href="#intro" class="scrollto"><h1 style="font-family: impact;font-size: 40px;color: #444461;">
                <b style="font-family: 'Montserrat', sans-serif;">ITDB Services</b></h1></a>
            </div>
        </div>
      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li class="active"><a href="#intro">Home</a></li>
          <li><a href="#about">Nosotros</a></li>
          <li><a href="#services">Servicios</a></li>          
          <li><a href="#cursos">Cursos</a></li>          
          <li><a href="#contact">Contacto</a></li>
        </ul>
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro" class="clearfix">
    <div class="container">

      <div class="intro-img">
        <img src="img/intro-img.svg" alt="" class="img-fluid">
      </div>

      <div class="intro-info">
        <h2>Administramos, Solucionamos e Innovamos en IT</h2>
        <!--<div>
          <a href="#about" class="btn-get-started scrollto">Get Started</a>
          <a href="#services" class="btn-services scrollto">Nuestros Servicios</a>
        </div>-->
      </div>

    </div>
  </section><!-- #intro -->

  <main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3>Nosotros</h3>
          <p>Disponemos de un equipo de consultores con más de 15 años de trayectoria en brindar soluciones integrales de IT y con una vasta experiencia en el mercado donde nos ponemos a
su disposición para colaborar conjuntamente y llevar a cabo proyectos tecnológicos exitosos.
</p>
        </header>

        <div class="row about-container">

          <div class="col-lg-6 content order-lg-1 order-2">
              <b>Nuestro Objetivo</b>
            <p>
            Es proveer servicios de calidad que ayuden a resolver problemas complejos de performance, arquitectura, disponibilidad, escalabilidad y desarrollo de aplicaciones web.  
            </p>

            <p><b>Valores</b></p>
            <div class="icon-box wow fadeInUp">
              <div class="icon"><i class="fa fa-check"></i></div>
              <br><h4 class="title">Excelencia</h4>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.2s">
              <div class="icon"><i class="fa fa-check"></i></div>
              <br><h4 class="title">Ética</h4>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
              <div class="icon"><i class="fa fa-check"></i></div>
              <br><h4 class="title">Compromiso</h4>
            </div>
            <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
              <div class="icon"><i class="fa fa-check"></i></div>
              <br><h4 class="title">Valoración del Capital Humano</h4>
            </div>
            <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
              <div class="icon"><i class="fa fa-check"></i></div>
              <br><h4 class="title">Energía creativa y coherencia</h4>
            </div>

          </div>

          <div class="col-lg-6 background order-lg-2 order-1 wow fadeInUp">
            <img src="img/about-img.svg" class="img-fluid" alt="">
          </div>
        </div>

        <div class="row about-extra">
          <div class="col-lg-6 wow fadeInUp">
            <img src="img/about-extra-1.svg" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 wow fadeInUp pt-5 pt-lg-0">
            <h4>Infraestructura</h4>
            <p>La infraestructura de toda compañía debe ser uno de los puntos más fuertes del negocio de la misma. Experimente con nuestra experiencia y obtenga resultados eficientes.</p>
            <h4>Cloud Computing</h4>
            <p>
               Es la nueva tecnología que medianas y grandes empresas están adoptando, donde tiende la mayoría de las soluciones informáticas. Pruébelo con nuestros Ingenieros más capacitados del país.
            </p>           
          </div>
        </div>

        <div class="row about-extra">
          <div class="col-lg-6 wow fadeInUp order-1 order-lg-2">
            <img src="img/about-extra-2.svg" class="img-fluid" alt="">
          </div>

          <div class="col-lg-6 wow fadeInUp pt-4 pt-lg-0 order-2 order-lg-1">            
            <h4>Capacitación</h4>
            <p>
            Brindamos formación académica in situ con horarios flexibles a sus profesionales, sobre las soluciones IT preponderantes del mercado informático.             
            </p>
            <h4>Te ayudamos a encontrar tu Solución IT</h4>
            <p>
            El espíritu de nuestro grupo, no es solo brindarte la solución para tu negocio, sino ayudarte a encontrar dicha solución y lo más conveniente  para así,  brindar réditos  a corto plazo.
            Te vamos a acompañar en todo el trayecto de elección, implementación, puesta en producción y mejoras a futuro, porque queremos ser parte del éxito de tu companía.
            </p>
          
          </div>
          
        </div>

      </div>
    </section><!-- #about -->

    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3>Servicios</h3>
          <p>En IT DB Services te brindamos un abanico de soluciones dentro de Infraestructura IT.</p>
        </header>

        <div class="row">

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <h4 class="title">Sistemas Operativos</h4>
              <p class="description">Este servicio de gran demanda, puede ser brindado con una modalidad remota o In House en las instalaciones de nuestros clientes. IT DB Services se compromete a cumplir estrictas normas y contratos de confidencialidad para el manejo de la información.</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <h4 class="title">Bases de Datos</h4>
              <p class="description">En todo negocio y área IT, las bases de datos cumplen un rol fundamental en el negocio, siendo una de las capas más bajas de la infraestructura, por lo tanto debe ser administrada de una forma proactiva y cumpliendo con todas la mejores prácticas.  </p>
            </div>
          </div>

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <h4 class="title">Backups & Restore</h4>
              <p class="description">En este campo de la infraestructura, con nuestra consultoría buscamos acompañar al cliente en la adopción de nuevas tecnologías, resolver inconvenientes técnicos muy específicos y cubrir la demanda operativa transitoria de la rama de Backups and Restore.</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <h4 class="title">Virtualización</h4>
              <p class="description">El sentido de la virtualización es brindar un Data Center accesible, flexible y escalable que responda de forma inmediata a los cambios constantes del sistema de negocio le garantiza una ventaja competitiva en el mercado actual.</p>
              <br>
            </div>              
          </div>

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1.4s">            
            <div class="box">
              <h4 class="title">Monitoreo</h4>
              <p class="description">El servicio de monitoreo que brindamos está diseñado con procedimientos estandarizados que cubren sus necesidades fundamentales de protección y control de todos los servidores e infraestructura en general de sus Data Center, convirtiendo a su alarma en un verdadero sistema de alerta.</p>
              <br><br>
            </div>
          </div>
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1.4s">
            <div class="box">
              <h4 class="title">Storage</h4>
              <p class="description">Las soluciones de IT DB Services ayudan a las organizaciones a transformar un conjunto de datos aislados, desestructurados o sin tratar, en valiosa información, ofreciéndolos con una visión organizada para su uso de una forma eficiente y que está preparada para ser usada en el modo en que sea necesario ya sea en entornos TI convencionales, virtualizados, o en el cloud.</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #services -->    

    <!--==========================
      Clients Section
    ============================-->
<section id="cursos" class="wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
      <div class="container">
        <header class="section-header">
          <h3>Cursos</h3>
          <p>Nuestros Cursos</p>
        </header>

        <div class="row row-eq-height justify-content-center">

          <div class="col-lg-6 mb-6">
            <div class="card wow bounceInUp" style="visibility: visible; animation-name: bounceInUp;">
              <div class="card-body">
                <img src="img/Oracle.png" alt="" class="img-fluid">
                <p class="card-text"><br>El curso comienza con la introducción a las bases de datos relacionales y las tareas que debe 
                    desempeñar un administrador de bases de datos. Luego a través de una Virtual Machine, los alumnos aprenderán paso por paso a realizar la instalación del Oracle. </p>
                
              </div>
            </div>
          </div>

          <div class="col-lg-6 mb-6">
            <div class="card wow bounceInUp" style="visibility: visible; animation-name: bounceInUp;">
                
              <div class="card-body">
                  <img src="img/MySQL.png" alt="" class="img-fluid">
                <p class="card-text"><br>El curso esta enfocado y orientado hacia administradores de bases y otros profesionales de bases de datos. 
Nuestros instructores universitarios le enseñarán como instalar y configurar el servidor MySQL, configurar la replicación y la seguridad, 
realizar copias de seguridad de bases de datos y la optimización del rendimiento y proteger las bases de datos MySQL.</p>                
              </div>
            </div>
          </div>

        </div>        
          <br><br>
        <div class="row row-eq-height justify-content-center">

          <div class="col-lg-6 mb-6">
            <div class="card wow bounceInUp" style="visibility: visible; animation-name: bounceInUp;">                
              <div class="card-body">
                <img src="img/PostgreSQL.png" alt="" class="img-fluid">
                <p class="card-text"><br>Los asistentes a nuestros cursos estarán capacitados para administrar de manera 
                     segura y eficiente el servidor de bases de datos PostgreSQL, además conocerán y practicarán la afinación (tunning) de una base de datos con una gran cantidad de registros. </p>
                
              </div>
            </div>
          </div>

          <div class="col-lg-6 mb-6">
            <div class="card wow bounceInUp" style="visibility: visible; animation-name: bounceInUp;">
              <div class="card-body">
                <img src="img/RedHat.png" alt="" class="img-fluid">
                <p class="card-text"><br>El curso forma a los asistentes que busquen convertirse en administradores de sistemas Linux.  Al finalizar este curso, los alumnos podrán administrar y solucionar problemas 
                    con los sistemas de archivos y las particiones, administrar volúmenes lógicos, realizar el control de acceso y gestionar paquetes, gestion y control de procesos, administracion de usuarios y grupos.</p>
              </div>
            </div>
          </div>

        </div>        

      </div>
    </section>

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact">
      <div class="container-fluid">

        <div class="section-header">
          <h3>Contacto</h3>
        </div>

        <div class="row wow fadeInUp">

            <div class="col-lg-2"></div>

          <div class="col-lg-8">
            <div class="row">              
              <div class="col-md-4 info">
                <i class="ion-ios-email-outline"></i>
                <p>info@itdbservices.com</p>
              </div>
              <div class="col-md-4 info">
                <i class="fa fa-whatsapp"></i>
                <a href="http://wa.me/5491168392764" target="_blank"><p>+54 9 11 6839-2764</p></a>
              </div>
              <div class="col-md-4 info">
                <i class="fa fa-whatsapp"></i>
                <a href="http://wa.me/5491155087014" target="_blank"><p>+54 9 11 5508-7014</p></a>
              </div>
            </div>

            <div class="form">
              <div id="sendmessage">Su mensaje fue enviado correctamente.</div>
              <div id="errormessage"></div>
              <!--<form method="post" action="" id="myform" name="myform" class="contactForm">-->
              <form action="" method="post" role="form" class="contactForm">
                <div class="form-row">
                  <div class="form-group col-lg-6">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Su nombre" data-rule="minlen:4" data-msg="Ingrese su nombre" />
                    <div class="validation"></div>
                  </div>
                  <div class="form-group col-lg-6">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Su email" data-rule="email" data-msg="Ingrese un email v�lido" />
                    <div class="validation"></div>
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Asunto" data-rule="minlen:4" data-msg="Por favor ingrese un asunto" />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="message" id="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Mensaje"></textarea>
                  <div class="validation"></div>
                </div>
                <div class="text-center"><button type="submit" title="Enviar Mensaje">Enviar</button></div>
              </form>
            </div>
          </div>
            
            <div class="col-lg-2"></div>

        </div>

      </div>
    </section><!-- #contact -->

  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <h1 style="font-family: impact;font-size: 40px">
                <b style="font-family: 'Montserrat', sans-serif;">ITDB Services</b>
            </h1>
            </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Enlaces Útiles</h4>
            <ul>
                <li><a href="#about">Nosotros</a></li>
                <li><a href="#services">Servicios</a></li>    
                <li><a href="#cursos">Cursos</a></li>          
                <li><a href="#contact">Contacto</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-8 footer-contact">
            <h4>Contacto</h4>
            <p>              
              <strong>Telefono: </strong><i class="fa fa-whatsapp"></i> +54 9 11 6839-2764<br>
              <strong>Telefono: </strong><i class="fa fa-whatsapp"></i> +54 9 11 5508-7014<br>
              <strong>Email:</strong> info@itdbservices.com<br>
            </p>

            <div class="social-links">              
                <a href="https://www.linkedin.com/in/itdb-services/" class="linkedin"><i class="fa fa-linkedin"></i></a>
                <a href="https://www.facebook.com/ITDBServices/" class="linkedin"><i class="fa fa-facebook"></i></a>
                <a href="https://www.instagram.com/itdbservices/" class="linkedin"><i class="fa fa-instagram"></i></a>
                <a href="https://twitter.com/ITDBServices" class="twitter"><i class="fa fa-twitter"></i></a>                
                <a href="http://wa.me/5491155087014" target="_blank"><i class="fa fa-whatsapp"></i></a>
                <a href="http://wa.me/5491168392764" target="_blank"><i class="fa fa-whatsapp"></i></a>
            </div>

          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>ITDB Services</strong>. All Rights Reserved
      </div>      
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/mobile-nav/mobile-nav.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/isotope/isotope.pkgd.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
